define([
'goo/math/Vector', 
'goo/math/Vector3',
'goo/math/Matrix3x3',
'goo/entities/components/TransformComponent'
], 
function (
	Vector,
	Vector3,
	Matrix3x3,
	TransformComponent
) {

	"use strict";

	function ThirdPersonCameraScript (target, properties) {
		this.target = target;
		this.properties = properties;

		this.cameraX = properties.cameraX ? properties.cameraX : 0.0;
		this.cameraY = properties.cameraY ? properties.cameraY : 0.0;
		this.cameraZ = properties.cameraZ ? properties.cameraZ : 0.0;
	};

	ThirdPersonCameraScript.prototype.run = function (entity) {

		if (!this.target) {
			console.log("ThirdPersonCameraScript has no target");
			return;
		}

		var fwdVector = new Vector3(0, 0, -1);
		var upVector = new Vector3(0, 1, 0);

		var targetTranslation = this.target.transformComponent.transform.translation;
		var targetRotation = this.target.transformComponent.transform.rotation;

		// Rotate the normalized fwd vector
		targetRotation.applyPost(fwdVector);
		targetRotation.applyPost(upVector);

		// Put the camera on the opposite side of the forward vector
		var cameraTranslation = entity.transformComponent.transform.translation;
		cameraTranslation.x = targetTranslation.x + this.cameraX;
		cameraTranslation.y = targetTranslation.y + this.cameraY;
		cameraTranslation.z = targetTranslation.z + this.cameraZ;

		// Vector to look at. Start at target.
		var lookAtMe = new Vector3(targetTranslation);
		// Scaled version of fwdVector
		fwdVector.scale(999);
		lookAtMe.add(fwdVector);

		//entity.transformComponent.transform.lookAt(lookAtMe, upVector);

		entity.transformComponent.setUpdated();

	};

	return ThirdPersonCameraScript;

});
