define([
'goo/math/Vector3',
'goo/entities/components/TransformComponent',
'goo/entities/components/ScriptComponent',
'goo/entities/components/MeshDataComponent',
'goo/entities/components/MeshRendererComponent',
'goo/entities/EntityUtils',
'goo/entities/Entity',
'goo/shapes/Sphere',
'goo/renderer/Material',
'goo/renderer/shaders/ShaderLib',
'lib/MotionScript'
],
function (
Vector3,
TransformComponent,
ScriptComponent,
MeshDataComponent,
MeshRendererComponent,
EntityUtils,
Entity,
Sphere,
Material,
ShaderLib,
MotionScript
) {

	"use strict";

	function WeaponScript (parentEntity, shots, goo, properties) {

		this.name = 'WeaponScript';
		this.properties = properties;
		this.shots = shots;
		this.goo = goo;

		this.fireKey = !isNaN(properties.fireKey) ? properties.fireKey : 32;
		this.firingRate = !isNaN(properties.firingRate) ? properties.firingRate : 10.0;
		this.shotSpeed = !isNaN(properties.shotSpeed) ? properties.shotSpeed : 30.0;

		this.parentEntity = parentEntity;

		this.shotMaterial = Material.createMaterial(ShaderLib.simpleLit, 'ShotMaterial');
		this.shotMaterial.uniforms.materialDiffuse = [1, 1, 0.5, 1];
		this.shotMaterial.uniforms.materialEmissive = [1, 1, 0.5, 1];

		this.shotMeshData = new Sphere(10, 10, 0.6); 

		// Use to alternate between left and right cannon
		this.leftCannon = true;

		// Firing state
		this.firing = false;
		this.firingTimer = 1.0/this.firingRate;

	};

	WeaponScript.prototype.updateKeys = function (event, down) {

		switch (event.keyCode) {
			case this.fireKey:
				this.firing = down ? true : false;
				break;
		}

	};

	WeaponScript.prototype.setupKeyControls = function() {

		var that = this;
		this.domElement.setAttribute('tabindex', -1);
		this.domElement.addEventListener('keydown', function (event) {
			that.updateKeys(event, true);
		}, false);

		this.domElement.addEventListener('keyup', function (event) {
			that.updateKeys(event, false);
		}, false);

	};

	WeaponScript.prototype.fire = function() {

		// Create an entity to
		var shotEntity = EntityUtils.createTypicalEntity(this.goo.world, this.shotMeshData);
		shotEntity.meshRendererComponent.materials.push(this.shotMaterial);
		shotEntity.transformComponent.setTranslation(this.parentEntity.transformComponent.worldTransform.translation);

		// Stretch the sphere for maximum coolness effect
		shotEntity.transformComponent.setScale(1, 1, 80);

		// Alternate between left and right cannon
		var xOffset = 8.0;
		if (this.leftCannon) xOffset *= -1;

		// Fine tuning of position
		var offset = new Vector3(xOffset, -2, -35);
		// Follow plane rotation
		this.parentEntity.transformComponent.transform.rotation.applyPost(offset);
		shotEntity.transformComponent.addTranslation(offset);

		// Send out into the world to cause destruction
		shotEntity.addToWorld();

		// Use MotionScript to move the shots
		var shotScriptComponent = new ScriptComponent();
		shotScriptComponent.scripts.push(new MotionScript({
			fwdVector: {x: 0, y: 0, z: -1},
			moveSpeed: this.shotSpeed,
			lifeTime: 1.5
		}));
		shotEntity.setComponent(shotScriptComponent);

		// Keep track of shots
		this.shots[shotEntity.id] = shotEntity;

		// Swap cannon
		this.leftCannon = !this.leftCannon;

	};

	WeaponScript.prototype.run = function (entity, tpf, env) {

		if (env) {
			if (!this.domElement && env.domElement) {
				this.domElement = env.domElement;
				this.setupKeyControls();
				console.log(this.domElement);
			}
		}

		this.firingTimer -= tpf;
		if (this.firing && this.firingTimer < 0.0) {
			this.fire();
			// Reset timer
			this.firingTimer = 1.0/this.firingRate; 
		}	

	};

	return WeaponScript;

});