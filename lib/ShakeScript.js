define([
    'goo/math/Vector3',
    'goo/entities/components/TransformComponent'
],
function (
    Vector3,
    TransformComponent
) {
    "use strict";

    function ShakeScript (properties) {
        this.name = "ShakeScript";
        this.properties = properties;
        this.magnitude = !isNaN(properties.magnitude) ? properties.magnitude : 0.1;
    };

    ShakeScript.prototype.run = function (entity, tpf, env) {

        // Calculate a small offset and apply to original transform.
        // This assumes that the entity is at (0, 0, 0) locally!
        var transformComponent = entity.transformComponent;
        var offset = new Vector3(this.magnitude*(-0.5 + Math.random()), 
                                 this.magnitude*(-0.5 + Math.random()),
                                 this.magnitude*(-0.5 + Math.random()));
        transformComponent.transform.translation = offset;
        transformComponent.setUpdated();
    };

    return ShakeScript;

});