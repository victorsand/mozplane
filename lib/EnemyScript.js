define([
'goo/entities/components/TransformComponent',
'goo/entities/components/ScriptComponent',
'goo/entities/components/MeshDataComponent',
'goo/entities/components/MeshRendererComponent',
'goo/entities/components/ParticleComponent',
'goo/entities/systems/ParticlesSystem',
'goo/entities/Entity',
'goo/entities/EntityUtils',
'goo/particles/ParticleUtils',
'goo/util/ParticleSystemUtils',
'goo/particles/ParticleLib',
'goo/math/Vector3',
'goo/shapes/Sphere',
'goo/renderer/Material',
'goo/renderer/shaders/ShaderLib',
'goo/renderer/Texture',
'goo/renderer/TextureCreator',
'lib/MotionScript'
],
function (
	TransformComponent,
	ScriptComponent,
	MeshDataComponent,
	MeshRendererComponent,
	ParticleComponent,
	ParticlesSystem,
	Entity,
	EntityUtils,
	ParticleUtils,
	ParticleSystemUtils,
	ParticleLib,
	Vector3,
	Sphere,
	Material,
	ShaderLib,
	Texture,
	TextureCreator,
	MotionScript
) {

	"use strict";

	function EnemyScript (planeEntity, enemies, shots, goo, properties) {

		this.name = 'EnemyScript';

		// Spawn from this entity
		this.planeEntity = planeEntity;

		this.properties = properties;

		// Keep track of enemies and shots in world
		this.enemies = enemies;
		this.shots = shots;

		// Reference to the world
		this.goo = goo;

		// Settings
		this.spawnRate = !isNaN(properties.spawnRate) ? properties.spawnRate : 10.0;
		this.speed = !isNaN(properties.speed) ? properties.speed : 10.0;
		this.distance = !isNaN(properties.distance) ? properties.distance : 300.0;
		this.size = !isNaN(properties.size) ? properties.size: 20.0;
		this.enemyHealth = !isNaN(properties.enemyHealth) ? properties.enemyHealth: 5;

		// Material and mesh
		this.enemyMeshData = new Sphere(40, 40, this.size); 

		// Keep track of time since last spawn (in seconds). Start from zero.
		this.spawnTimer = 0.0;

		// For explosion particles
		this.explosionMaterial = Material.createMaterial(ShaderLib.particles);
		this.explositionTexture = new TextureCreator().loadTexture2D('res\\flare.png'); 
		this.explositionTexture.generateMipmaps = true;
		this.explosionMaterial.setTexture('DIFFUSE_MAP', this.explositionTexture);
		this.explosionMaterial.blendState.blending = 'AdditiveBlending';
		this.explosionMaterial.cullState.enabled = false;
		this.explosionMaterial.depthState.write = false;
		this.explosionMaterial.renderQueue = 2001;
	};

	// Send an enemy into the world
	EnemyScript.prototype.spawn = function() {

		// We like colorful enemies!
		var randomColorMaterial = Material.createMaterial(ShaderLib.uber, 'EnemyMaterial');
		randomColorMaterial.uniforms.materialDiffuse = [Math.random(), Math.random(), Math.random(), 1];
		randomColorMaterial.uniforms.materialEmmisive = [Math.random(), Math.random(), Math.random(), 1];

		var enemyEntity = EntityUtils.createTypicalEntity(this.goo.world, this.enemyMeshData);
		enemyEntity.meshRendererComponent.materials.push(randomColorMaterial);
		// Randomize the x position a little bit
		var xOffset = -50+Math.random()*100;
		// Z position relative to plane
		enemyEntity.transformComponent.setTranslation(new Vector3(xOffset,
																  this.planeEntity.transformComponent.transform.translation.y-3, // weapon level
																  this.planeEntity.transformComponent.transform.translation.z-this.distance));

		enemyEntity.addToWorld();

		// Throw the enemies against us
		var enemyScriptComponent = new ScriptComponent();
		enemyScriptComponent.scripts.push(new MotionScript({
			fwdVector: {x: 0, y: 0, z: 1},
			moveSpeed: this.speed,
			lifeTime: 20.0
		}));
		enemyEntity.setComponent(enemyScriptComponent);

		enemyEntity.health = this.enemyHealth;

		// Keep track of enemies for simple collision handling
		this.enemies[enemyEntity.id] = enemyEntity;
	};

	EnemyScript.prototype.explodeEnemy = function (enemyEntity) {

		// Entity for the particles
		var particlesEntity = EntityUtils.createTypicalEntity(this.goo.world);

		// A fire-looking particle system
		var particleSystemEntity = ParticleSystemUtils.createParticleSystemEntity(
			this.goo,
		    {
				totalParticlesToSpawn : 20,
				releaseRatePerSecond : 30,
				minLifetime : 0.5,
				maxLifetime : 2.0,
				getEmissionVelocity : function (particle, particleEntity) {
					var vec3 = particle.velocity;
					return ParticleUtils.getRandomVelocityOffY(vec3, 0, Math.PI * 15 / 180, 5);
				},
				timeline : [{
					timeOffset : 0.0,
					spin : 0,
					mass : 20,
					size : 30.0,
					color : [1, 1, 0, 1]
				}, {
					timeOffset : 0.25,
					color : [1, 0, 0, 1]
				}, {
					timeOffset : 0.25,
					color : [0, 0, 0, 1]
				}, {
					timeOffset : 0.5,
					size : 50.0,
					color : [0, 0, 0, 0]
				}]
			} 
		,this.explosionMaterial);

		particleSystemEntity.transformComponent.setTranslation(enemyEntity.transformComponent.transform.translation);
		particleSystemEntity.addToWorld();
	};

	EnemyScript.prototype.run = function (entity, tpf, env) {

		// Get a hold of the DOM
		if(env) {
			if (!this.domElement && env.domElement) {
				this.domElement = env.domElement;
			}
		}

		// Subtract time since last frame from timer, spawn if timer has run down
		this.spawnTimer -= tpf;
		if (this.spawnTimer < 0.0) {
			this.spawn();
			// Reset the timer
			this.spawnTimer = 1.0/this.spawnRate;
		}

		// Do a simple (inefficient) shot/enemy collision handling.
		// This should probably be in a separate class or script, but we stick it here
		// for simplicity in the tutorial.
		for (var shot_key in this.shots) {
			var shot = this.shots[shot_key];
			// Remove timed out shots
			if (shot.dead) {
				shot.removeFromWorld();
				delete this.shots[shot_key];
			} else {
				for (var enemy_key in this.enemies) {
					var enemy = this.enemies[enemy_key];
					// Remove times out enemies
					if (enemy.dead) {
						enemy.removeFromWorld();
						delete this.enemies[enemy_key];
					}
					// Compare positions
					var shotPos = shot.transformComponent.worldTransform.translation;
					var enemyPos = enemy.transformComponent.worldTransform.translation;
					if (Vector3.distance(shotPos, enemyPos) < this.size) {
						enemy.health -= 1;
						if (enemy.health < 0) {
							this.explodeEnemy(enemy);
							enemy.removeFromWorld();
							delete this.enemies[enemy_key];
							// Stop looping, or you will have a bad time
							break;
						}
						shot.removeFromWorld();
						delete this.shots[shot_key];
					}
				}
			}
		}

	};

	return EnemyScript;

});