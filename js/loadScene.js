require([
'goo/statemachine/FSMSystem',
'goo/loaders/DynamicLoader',
'goo/math/Vector3',
'goo/entities/GooRunner',
'goo/entities/Entity',
'goo/entities/EntityUtils',
'goo/entities/components/CameraComponent',
'goo/entities/components/LightComponent',
'goo/entities/components/ScriptComponent',
'goo/shapes/Sphere',
'goo/renderer/Camera',
'goo/renderer/light/DirectionalLight',
'goo/renderer/Material',
'goo/renderer/shaders/ShaderLib',
'goo/scripts/MouseLookControlScript',
'goo/scripts/WASDControlScript',
'lib/MotionScript',
'lib/WeaponScript',
'lib/ShakeScript',
'lib/EnemyScript',
'lib/TiltScript',
'goo/util/Grid',
], function (
FSMSystem,
DynamicLoader,
Vector3,
GooRunner,
Entity,
EntityUtils,
CameraComponent,
LightComponent,
ScriptComponent,
Sphere,
Camera,
DirectionalLight,
Material,
ShaderLib,
MouseLookControlScript,
WASDControlScript,
MotionScript,
WeaponScript,
ShakeScript,
EnemyScript,
TiltScript,
Grid
) {

	'use strict';

	function init() {

		// If you try to load a scene without a server, you're gonna have a bad time
		if (window.location.protocol==='file:') {
			alert('You need to run this webpage on a server. Check the code for links and details.');
			return;
		}

		var worldWidth = 100;
    	var worldLength = 20000;

    	// Things we need to keep track of globally
    	var shots = []
    	var enemies = []

    	// Make a grid to act as a floor
    	var initGrid = function() {
    		var grid = new Grid(goo.world, {
				grids: [{
					stepX: 12.5,
					stepY: 12.5,
					color: [0.2, 0.2, 0.3, 1],
				}],
				floor: true,
				fogOn: false,
				width:  worldWidth,
				height: worldLength
			});
    		grid.addToWorld();
    	}

    	var initCamera = function(parentEntity) {

    		// Create an entity to hold the camera
    		var cameraEntity = goo.world.createEntity('Camera');

    		// Create new camera (fov, aspect, near, far)
    		var camera = new Camera(35, 1, 0.1, 1000);

    		// Attach the camera to the entity
    		cameraEntity.setComponent(new CameraComponent(camera));

			// Attach to parent
			parentEntity.transformComponent.attachChild(cameraEntity.transformComponent);

			// Place the camera behind the plane
			cameraEntity.transformComponent.addTranslation(0, 10, 70);

    		// Add the camera to the world
    		cameraEntity.addToWorld();
    	}

    	var initPlane = function(planeParentEntity, planeEntity) {

    		// Scale the mesh and put the plane at a nice starting position
    		var scale = 1.0;
			planeEntity.transformComponent.setScale(new Vector3(scale, scale, scale));
			planeParentEntity.transformComponent.setTranslation(new Vector3(0.0, 20, worldLength/2.0));

			// Some scripts on parent, some on plane
			var planeScriptComponent = new ScriptComponent();
			var planeParentScriptComponent = new ScriptComponent();

			// Init simple WASD controls using built in script
			planeParentScriptComponent.scripts.push(new WASDControlScript( {
				walkSpeed: 50.0
			}));		

			// Set up a custom movement script
			planeParentScriptComponent.scripts.push(new MotionScript({
				fwdVector: {x: 0, y: 0, z: -1},
				moveSpeed: 1.5
			}));

			// Set up weapons relative to plane
			planeScriptComponent.scripts.push(new WeaponScript(planeEntity, shots, goo, {
				shotSpeed: 30.0,
				firingRate: 15.0
    		}));

			// Set up enemies relative to parent
			planeParentScriptComponent.scripts.push(new EnemyScript(planeParentEntity, enemies, shots, goo, {
    			speed: 4.0,
    			spawnRate: 0.6,
    			distance: 2000,
    			size: 20.0,
    			enemyHealth: 10
    		}));

    		// Set up tilting awesomeness for plane
    		planeScriptComponent.scripts.push(new TiltScript({ 
    			maxTilt: Math.PI/4.0,
    			tiltSpeed: 0.05

    		}));

			// Tie the scripts to the entites
			planeEntity.setComponent(planeScriptComponent);
			planeParentEntity.setComponent(planeParentScriptComponent)
    	}

    	var initExhaust = function(planeEntity) {

    		// Make two different fire materials
    		var outerFireMaterial = Material.createEmptyMaterial(ShaderLib.uber, 'outerFireMaterial');
    		outerFireMaterial.uniforms.materialDiffuse = [0, 0, 0, 0];
    		outerFireMaterial.uniforms.materialSpecular = [0, 0, 0, 0];
    		outerFireMaterial.uniforms.materialEmissive = [0.03, 0.03, 0.01, 0.2];
    		outerFireMaterial.blendState.blending = 'CustomBlending';
    		outerFireMaterial.blendState.blendEquation = 'AddEquation';
    		outerFireMaterial.blendState.blendSrc = 'SrcAlphaFactor';
    		outerFireMaterial.blendState.blendDst = 'OneFactor';
    		outerFireMaterial.depthState.enabled = false;

    		// Connect dummy nodes behind the exhausts to connect the 'flames' to
    		var leftDummy = EntityUtils.createTypicalEntity(goo.world);
    		planeEntity.transformComponent.attachChild(leftDummy.transformComponent);
    		leftDummy.transformComponent.addTranslation(new Vector3(0.9, 0.0, 22.0));
    		leftDummy.addToWorld();

    		var rightDummy = EntityUtils.createTypicalEntity(goo.world);
    		planeEntity.transformComponent.attachChild(rightDummy.transformComponent);
    		rightDummy.transformComponent.addTranslation(new Vector3(-0.9, 0.0, 22.0));
    		rightDummy.addToWorld();

    		// Use elongated spheres as flames. All entities can share the same mesh data.
    		var sphereMesh = new Sphere(30, 30, 30);

    		// Create the flame entities
    		var flames = []

    		var leftOuterFlame = EntityUtils.createTypicalEntity(goo.world, sphereMesh);
    		leftOuterFlame.meshRendererComponent.materials.push(outerFireMaterial);
    		leftDummy.transformComponent.attachChild(leftOuterFlame.transformComponent);
    		leftOuterFlame.transformComponent.setScale(new Vector3(0.025, 0.025, 0.3));
    		leftOuterFlame.addToWorld();
    		flames.push(leftOuterFlame);

    		var rightOuterFlame = EntityUtils.createTypicalEntity(goo.world, sphereMesh);
    		rightOuterFlame.meshRendererComponent.materials.push(outerFireMaterial);
    		rightDummy.transformComponent.attachChild(rightOuterFlame.transformComponent);
    		rightOuterFlame.transformComponent.setScale(new Vector3(0.025, 0.025, 0.3));
    		rightOuterFlame.addToWorld();
    		flames.push(rightOuterFlame);

    		var leftInnerFlame = EntityUtils.createTypicalEntity(goo.world, sphereMesh);
    		leftInnerFlame.meshRendererComponent.materials.push(outerFireMaterial);
    		leftDummy.transformComponent.attachChild(leftInnerFlame.transformComponent);
    		leftInnerFlame.transformComponent.setScale(new Vector3(0.015, 0.015, 0.2));
    		leftInnerFlame.addToWorld();
    		flames.push(leftInnerFlame);

    		var rightInnerFlame = EntityUtils.createTypicalEntity(goo.world, sphereMesh);
    		rightInnerFlame.meshRendererComponent.materials.push(outerFireMaterial);
    		rightDummy.transformComponent.attachChild(rightInnerFlame.transformComponent);
    		rightInnerFlame.transformComponent.setScale(new Vector3(0.015, 0.015, 0.2));
    		rightInnerFlame.addToWorld();
    		flames.push(rightInnerFlame);

    		// Attach a script to all flames to shake it up a little!
    		// This is the reason we stored all the flames in an array.
			var shakeScriptComponent = new ScriptComponent();
			shakeScriptComponent.scripts.push(new ShakeScript({}));
			for (var i=0; i<flames.length; i++) {
				flames[i].setComponent(shakeScriptComponent);
			}

    	}


		// Create typical Goo application
		var goo = new GooRunner({
			antialias: true
		});
		goo.world.setSystem(new FSMSystem(goo));
		goo.renderer.domElement.id = 'goo';
		document.body.appendChild(goo.renderer.domElement);

		// The Loader takes care of loading data from a URL...
		var loader = new DynamicLoader({world: goo.world, rootPath: 'res'});

		initGrid();

		// Load the project from the bundle exported by Goo Create
		loader.loadFromBundle('project.project', 'root.bundle')
		.then(function(configs) {

			// Make a parent entity for the plane. The parent entity enables a steady camera
			// while rotating the plane.
			var planeParentEntity = EntityUtils.createTypicalEntity(goo.world);
			planeParentEntity.addToWorld();

			// Get a hold of the loaded plane
			var planeEntity = loader.getCachedObjectForRef('Eurofighter/entities/RootNode.entity');

			planeParentEntity.transformComponent.attachChild(planeEntity.transformComponent);

			// Run the init functions
			initPlane(planeParentEntity, planeEntity);
			initExhaust(planeEntity);
			initCamera(planeParentEntity);

		})
		.then(null, function(e) {
			// The second parameter of 'then' is an error handling function.
			// We just pop up an error message in case the scene fails to load.
			alert('Failed to load scene: ' + e);
		});
	}

	init();

});
