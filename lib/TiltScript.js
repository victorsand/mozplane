define([
	'goo/entities/components/TransformComponent',
	'goo/math/Vector3'
],
function (
	TransformComponent,
	Vector3
) {

	"use strict";

	function TiltScript (properties) {

		this.name = 'TiltScript';
		this.properties = properties;
		this.maxTilt = !isNaN(properties.maxTilt) ? properties.maxTilt : Math.PI/4.0;
		this.tiltSpeed = !isNaN(properties.tiltSpeed) ? properties.tiltSpeed : 0.05;
		this.tilt = 0.0;

	};

	TiltScript.prototype.run = function (entity, tpf, env) {

		var xPos = entity.transformComponent.worldTransform.translation.x;
		if (!this.lastX) {
			this.lastX = xPos;
			return;a 
		}

		// Decrease tilt if moving left, increase if moving right
		if (xPos - this.lastX > 0) {
			this.tilt -= this.tiltSpeed;
		} else if (xPos - this.lastX < 0) {
			this.tilt += this.tiltSpeed;
		} else if (this.tilt != 0) {
			// If no movement, ease back towards zero
			this.tilt -= this.tiltSpeed*(this.tilt/this.maxTilt);
		} 

		// Cap tilt
		if (this.tilt < -this.maxTilt) {
			this.tilt = -this.maxTilt;
		} else if (this.tilt > this.maxTilt) {
			this.tilt = this.maxTilt;
		}

		entity.transformComponent.setRotation(0, 0, this.tilt);

		this.lastX = xPos;
	
		entity.transformComponent.setUpdated();

	};

	return TiltScript;

});