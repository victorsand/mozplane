define([
'goo/entities/components/TransformComponent',
'goo/math/Vector3'
],
function (
    TransformComponent,
    Vector3
) {

    "use strict";

    function MotionScript (properties) {

        this.properties = properties;
        this.fwdVector = properties.fwdVector ? properties.fwdVector : new Vector3(0, 0, -1);
        this.moveSpeed = properties.moveSpeed ? properties.moveSpeed : 1.0;
        this.lifeTime = properties.lifeTime ? properties.lifeTime : Number.MAX_VALUE;

    };

    MotionScript.prototype.run = function (entity, tpf, env) {

        entity.transformComponent.transform.translation.x += this.moveSpeed*this.fwdVector.x;
        entity.transformComponent.transform.translation.y += this.moveSpeed*this.fwdVector.y;
        entity.transformComponent.transform.translation.z += this.moveSpeed*this.fwdVector.z;

        // Count towards the death of the entity
        // Note that actual removal is not handled here!
        this.lifeTime -= tpf;
        if (this.lifeTime < 0) {
            entity.dead = true;
        }

        // Tell the engine to update the transform
        entity.transformComponent.setUpdated();

    };

    return MotionScript;

});